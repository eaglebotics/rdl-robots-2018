package org.firstinspires.ftc.teamcode.Robots;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;
import java.util.*;

public abstract class BigBoy extends BasicRobot {

    private static Servo leftGrabber, rightGrabber;
    private static boolean firstRun = true;

    private static void moveLeftGrabber(double degrees) {
        leftGrabber.setPosition(degrees);
    }
    private static void moveRightGrabber(double degrees) {
        rightGrabber.setPosition(degrees);
    }
    private static void openLeftGrabber(boolean open) {
        if (open) {
            moveLeftGrabber(.9);
        } else {
            moveLeftGrabber(.2);
        }
    }
    private static void openRightGrabber(boolean open) {
        if (open) {
            moveRightGrabber(.2);
        } else {
            moveRightGrabber(.9);
        }
    }

//    private static void strafeLeft() {
//        rightFrontWheel.setPower(-1);
//        leftFrontWheel.setPower(1);
//        rightBackWheel.setPower(-1);
//        leftBackWheel.setPower(-1);
//    }
//    private static void strafeRight() {
//        rightFrontWheel.setPower(1);
//        leftFrontWheel.setPower(-1);
//        rightBackWheel.setPower(1);
//        leftBackWheel.setPower(1);
//    }
    private void mecanum() {
        rightBackWheel.setDirection(DcMotorSimple.Direction.REVERSE);
        leftFrontWheel.setDirection(DcMotorSimple.Direction.REVERSE);

        double x = this.gamepad1.left_stick_x;
        double y = -this.gamepad1.left_stick_y;
        double z = this.gamepad1.right_stick_x;
        telemetry.addData("tl", (x + y + z) * .75);
        telemetry.addData("tr", (-x + y - z) * .75);
        telemetry.addData("bl", (-x + y + z) * .75);
        telemetry.addData("br", (x + y - z) * .75);
        leftFrontWheel.setPower((x + y + z) * .75);
        rightFrontWheel.setPower((x + y - z) * .75);
        leftBackWheel.setPower((-x + y + z) * .75);
        rightBackWheel.setPower((-x + y - z) * .75);

//        if (this.gamepad1.right_stick_y < 0 && Math.abs(this.gamepad1.right_stick_y) >= Math.abs(this.gamepad1.right_stick_x)) {
//            moveForward(Math.abs(gamepad1.right_stick_y));
//        } else if (this.gamepad1.right_stick_y > 0 && Math.abs(this.gamepad1.right_stick_y) >= Math.abs(this.gamepad1.right_stick_x)) {
//            moveBackward(Math.abs(gamepad1.right_stick_y));
//        }
    }

    /*
        The following protected methods are designed to
        be called from within an OpMode class.
     */

    @Override
    protected void teleOp() {
        leftBackWheel.setDirection(DcMotorSimple.Direction.REVERSE);
        mecanum();

        if (gamepad1.a) {
            openRightGrabber(false);
            openLeftGrabber(false);
        }
        if (gamepad1.y) {
            openLeftGrabber(true);
            openRightGrabber(true);
        }

        if (gamepad1.left_trigger > 0) {
            moveLeftGrabber(Math.abs(gamepad1.left_trigger));
        }
        if (gamepad1.right_trigger > 0) {
            moveRightGrabber(Math.abs(gamepad1.right_trigger));
        }

//        if (gamepad1.right_trigger > 0 && gamepad1.left_trigger > 0) {
//            openRightGrabber(false);
//            openLeftGrabber(false);
//        }

        if (gamepad1.left_bumper) openLeftGrabber(true);
        if (gamepad1.right_bumper) openRightGrabber(true);

        if (gamepad1.dpad_down) {
            moveLift(0.75);
        } else if (gamepad1.dpad_up) {
            moveLift(-0.75);
        } else{
            moveLift(0);
        }


//        if (gamepad1.left_stick_y < 0 && Math.abs(gamepad1.left_stick_y) >= Math.abs(gamepad1.left_stick_x)) {
//            moveForward();
//        } else if (gamepad1.left_stick_y > 0 && Math.abs(gamepad1.left_stick_y) >= Math.abs(gamepad1.left_stick_x)) {
//            // Backward
//            moveBackward();
//        } else if (gamepad1.left_stick_x < 0 && Math.abs(gamepad1.left_stick_y) <= Math.abs(gamepad1.left_stick_x)) {
//            turnRight();
//        } else if (gamepad1.left_stick_x > 0 && Math.abs(gamepad1.left_stick_y) <= Math.abs(gamepad1.left_stick_x)) {
//            turnLeft();
//        } else if (gamepad1.right_stick_x < 0) {
//            strafeRight();
//        } else if (gamepad1.right_stick_x > 0) {
//            strafeLeft();
//        } else {
//            // All motors off
//            setPowers(0);
//        }

    }

    @Override
    protected void initHardware() {
        super.initHardware();
        leftGrabber = hardwareMap.servo.get("lg");
        rightGrabber = hardwareMap.servo.get("rg");
    }

    @Override
    protected void auto() {
        leftBackWheel.setDirection(DcMotorSimple.Direction.REVERSE);
        moveForward(2, 1);
    }
}