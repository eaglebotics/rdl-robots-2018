package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.Robots.LongBoye;

@TeleOp(name = "LongBoyeTeleOp")

public class LongBoyeTeleOp extends LongBoye {

    @Override
    public void runOpMode() {
        initHardware();
        // telemetry is basically System.out.println. It's only really useful for when we put sensors on the bot (then we relay the info back to us)
        telemetry.addData("Status", "Initialized");
        telemetry.update();
        // Wait for the game to start (driver presses PLAY)
        waitForStart();

        // run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {
            // Checks if the controller D-Pad (gamepad1) is pushed up/down/left/right
            teleOp();
            telemetry.addData("Status", "Running");
            telemetry.update();

        }
    }
}