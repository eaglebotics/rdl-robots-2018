package org.firstinspires.ftc.teamcode.Robots;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

public abstract class LongBoye extends BasicRobot {

    static void turnLeft(double pwr) { setPowers(-pwr, -pwr); }
    static void turnRight(double pwr) { setPowers(pwr, pwr); }

    @Override
    protected void teleOp() {
        System.out.println(gamepad1.left_stick_x);
        System.out.println(gamepad1.right_stick_x);
        System.out.println(gamepad1.left_stick_y);
        System.out.println(gamepad1.right_stick_y);

        if (gamepad1.left_stick_y < 0 && Math.abs(gamepad1.left_stick_y) >= Math.abs(gamepad1.left_stick_x)) {
            moveForward(Math.abs(gamepad1.left_stick_y));
        } else if (gamepad1.left_stick_y > 0 && Math.abs(gamepad1.left_stick_y) >= Math.abs(gamepad1.left_stick_x)) {
            // Backward
            moveBackward(Math.abs(gamepad1.left_stick_y));
        } else if (gamepad1.left_stick_x < 0 && Math.abs(gamepad1.left_stick_y) <= Math.abs(gamepad1.left_stick_x)) {
            turnRight(Math.abs(gamepad1.left_stick_x));
        } else if (gamepad1.left_stick_x > 0 && Math.abs(gamepad1.left_stick_y) <= Math.abs(gamepad1.left_stick_x)) {
            turnLeft(Math.abs(gamepad1.left_stick_x));
        } else if (gamepad1.dpad_down) {
            moveLift(0.75);
        } else if (gamepad1.dpad_up) {
            moveLift(-0.75);
        } else {
            // No keys pressed (turn motors off)
            setPowers(0);
            moveLift(0);
        }
    }

    @Override
    public void auto() {
        moveForward(2, 1);
    }
}
