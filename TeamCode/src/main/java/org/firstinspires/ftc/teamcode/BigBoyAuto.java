package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.Robots.BigBoy;

@Autonomous (name = "BigBoyAuto")
public class BigBoyAuto extends BigBoy {
    @Override
    public void runOpMode() {
        initHardware();
        // telemetry is basically System.out.println. It's only really useful for when we put sensors on the bot (then we relay the info back to us)
        telemetry.addData("Status", "Initialized");
        telemetry.update();
        // Wait for the game to start (driver presses PLAY)
        waitForStart();

        auto();
        telemetry.addData("Status", "Running");
        telemetry.update();
    }
}
