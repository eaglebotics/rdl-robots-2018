package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.TeleOp;       // As opposed to Autonomous

import org.firstinspires.ftc.teamcode.Robots.BigBoy;

@TeleOp(name = "BigBoyTeleOp")

public class BigBoyTeleOp extends BigBoy {

    @Override
    public void runOpMode() {
        initHardware();
        // Telemetry is kind of like System.out.println, in that you can print data to the driver station
        telemetry.addData("Status", "Initialized");
        telemetry.update();
        // Wait for the game to start (driver presses PLAY)
        waitForStart();

        // run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {
            // Checks if the controller (named gamepad1) D-Pad is engaged up, down, left, or right.
            teleOp();
            telemetry.addData("Status", "Running");
            telemetry.update();

        }
    }
}